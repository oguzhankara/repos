import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';

class GoogleMaps extends StatefulWidget {
  GoogleMaps({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _GoogleMapsState createState() => new _GoogleMapsState();
}


class _GoogleMapsState extends State<GoogleMaps> {
  Completer<GoogleMapController> _controller = Completer();
  final key = new GlobalKey<ScaffoldState>();
  bool selectedData = true;
  Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  static const LatLng _center = const LatLng(41.0211216, 29.0019218);
  final Set<Marker> _markers = {};
  final Map<String, Marker> _marker = {};
  LatLng _lastMapPosition = _center;
  MapType _currentMapType = MapType.normal;

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
    });
  }
  void _onCameraMove(CameraPosition position) {
    _lastMapPosition = position.target;
  }
  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }
  void _getLocation() async {
    var currentLocation = await Geolocator()
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best);
    final GoogleMapController controller = await _controller.future;

    setState(() {
      markers.clear();
      final MarkerId markerId1 = MarkerId("curr_loc");
      final marker = Marker(
        markerId: markerId1,
        position: LatLng(currentLocation.latitude, currentLocation.longitude),
        infoWindow: InfoWindow(
            title: 'Sizin konumunuz',
            snippet: 'Buradasınız.' +
                currentLocation.latitude.toString() +
                " " +
                currentLocation.longitude.toString()),
      );
      markers[markerId1] = marker;
      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(
            target: LatLng(currentLocation.latitude, currentLocation.longitude),
            zoom: 17,
          ),
        ),
      );
    });
  }

  @override
  Widget build(BuildContext context) {

    Future<bool> _willPopCallback() async {
      Navigator.of(context).pop(false);
      return false;
    }

    Set<Marker> markerss = Set();
    markerss.addAll([
      Marker(
          markerId: MarkerId('value'),
          position: LatLng(41.0334971, 28.7866158)),
      Marker(
          markerId: MarkerId('value2'),
          position: LatLng(41.0385482, 28.7917019)),

    ]);
    return WillPopScope(
      onWillPop: _willPopCallback,
        child: Scaffold(
        key:key,
        appBar: AppBar(
          title: Text('Daimia'),
          backgroundColor: Colors.green[700],
        ),
        body: Stack(
          children: <Widget>[
            GoogleMap(
              onMapCreated: _onMapCreated,
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 11.0,
              ),
              myLocationEnabled: true,
              mapType: _currentMapType,
              //markers: _markers,
              markers: markerss,
              //markers: _marker.values.toSet(),
              onCameraMove: _onCameraMove,
            ),
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Align(
                alignment: Alignment.bottomRight,
                child: Column(
                  children: <Widget>[
                    FloatingActionButton(
                      onPressed: _onMapTypeButtonPressed,
                      materialTapTargetSize: MaterialTapTargetSize.padded,
                      backgroundColor: Colors.green,
                      child: const Icon(Icons.map, size: 36.0),
                    ),
                    SizedBox(height: 16.0),
                    FloatingActionButton(
                      onPressed: _getLocation,
                      materialTapTargetSize: MaterialTapTargetSize.padded,
                      backgroundColor: Colors.green,
                      child: const Icon(Icons.location_on, size: 36.0),
                    ),
                    SizedBox(height: 16.0),
                    FloatingActionButton(
                      onPressed:(null),
                      materialTapTargetSize: MaterialTapTargetSize.padded,
                      backgroundColor: Colors.green,
                      child: const Icon(Icons.directions, size: 36.0),
                    ),
                    SizedBox(height: 16.0),
                  ],
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}
